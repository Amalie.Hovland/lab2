package INF101.lab2;

import java.util.List;
import java.util.ArrayList;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    public ArrayList<FridgeItem> FridgeItems; // list of all FridgeItem objects in Fridge
    public int total_size;

    public Fridge(){
        this.FridgeItems = new ArrayList<FridgeItem>();
        this.total_size = 20; // set maximum fridge size to 20 items
    }

    @Override
    public int nItemsInFridge() {
        return FridgeItems.size();
    }

    @Override
    public int totalSize() {
        return total_size;
    }


    @Override
    public boolean placeIn(FridgeItem item) {
        if (FridgeItems.size() < total_size) {
            FridgeItems.add(item); //place in fridge as long as there is room
            return true;
        }
        else {
            return false;
        }
    }


    @Override
    public void takeOut(FridgeItem item) {
        if (FridgeItems.contains(item)) {
            FridgeItems.remove(item);
        }
        else { // error is raised if the fridge does not contain the item
            throw new NoSuchElementException(item.getName() + " could not be found in fridge.");
        }
    }

    @Override
    public void emptyFridge() {
        FridgeItems.removeAll(FridgeItems);
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> rmItems = new ArrayList<>();
        for (FridgeItem testExpired : FridgeItems) { // finds all expired items in fridge
            if (testExpired.hasExpired()) {
                rmItems.add(testExpired);
            }
        }
        FridgeItems.removeAll(rmItems); // removes expired items
        return rmItems; // returns items removed because of expiration date
    }
}